// Version 1.0.0

/** ******************************************************/
/**	ARCHER DFE APPROVED LIBRARIES
/********************************************************/
const request = require('request');
const { Builder } = require('xml2js');

// ++++++++++++++++++++ Development Only ++++++++++++++++++++
// Remove to deploy

// This is a relative path
// You may need to adjust path based on your file location
// const context = require('../../DevTools/DevCallBack.js');

// const { callback } = context;

// End Remove to deploy
// ++++++++++++++++ End Development Only +++++++++++++++++++

/** ******************************************************/
/** GLOBAL VARS
/********************************************************/
// Used only for Write To Disk
const outputWriter = context.OutputWriter.create('XML', { RootNode: 'ROOT' });
/* Supported datasources: allcontacts */
const transportSettings = {
    proxy: '',
    verifyCerts: 'false',
    dataSource: '',
    ignoreLastRunTime: 'false',
    batchSize: '1000',
    url: '',
    username: '',
    password: '',
    startDate: '',
    lastRunTimeOffset: '-1',
    requestsPerMin: '60',
    tokens: {},
    lastRunTime: new Date('1970-01-10T00:00:00Z'),
    runTime: new Date(),
    debug: false,
	instance: ''
};

/** ******************************************************/
/** DEBUGGING/TESTING SETTINGS
/********************************************************/
const testingModeTokens = {
    LastRunTime: '', // '2018-06-13T18:31:41Z',
    PreviousRunContext: ''
};

const testingModeParams = {
    url: 'InsertBaseURL',
    username: '***',
    password: '***',
    dataSource: '***',
    ignoreLastRunTime: 'false',
    startDate: '***'
};

/** ******************************************************/
/** ARCHER CALLBACK INTERFACE
/********************************************************/
const messageLogMap = new Map();

function pad(num, size) {
    let s = `${num}`;
    while (s.length < size) s = `0${s}`;
    return s;
}

function getDateTime() {
    const dt = new Date();
    return (
        `${pad(dt.getFullYear(), 4)}-` +
        `${pad(dt.getMonth() + 1, 2)}-` +
        `${pad(dt.getDate(), 2)} ` +
        `${pad(dt.getHours(), 2)}:` +
        `${pad(dt.getMinutes(), 2)}:` +
        `${pad(dt.getSeconds(), 2)}`
    );
}

function LogMessage(text, level) {
    let logMap = messageLogMap.get(level);
    if (!logMap) {
        logMap = messageLogMap.set(level, []).get(level);
    }
    logMap.push(text);
    if (logMap.length > 30 && level !== 'ERROR') {
        logMap.splice(0, logMap.length - 30);
    }
    console.log(`${getDateTime()} :: ${level}  :: ${text}`);
}

function LogInfo(text) {
    LogMessage(text, 'INFO');
}
function LogError(text) {
    LogMessage(text, 'ERROR');
}
function LogWarn(text) {
    LogMessage(text, 'WARN');
}

function CaptureError(err) {
    if (err != null) {
        let { stack } = err;
        const { message } = err;
        if (!stack) {
            /* create a new error to get the stack */
            const e = new Error();
            ({ stack } = e);
        }
        /* create error string for array */
        const errString = `${message}\n${stack}`;
        /* add to error array */
        LogError(errString);
    }
}

function jsonToXMLString(json, rootElement = null) {
    const bldrOpts = {
        headless: true,
        rootName: rootElement,
        renderOpts: { pretty: true, indent: '    ', newline: '\r\n', cdata: true }
    };
    // can't pass null to the 3rd party lib
    if (!rootElement) {
        delete bldrOpts.rootName;
    }
    return new Builder(bldrOpts).buildObject(json);
}

function jsonArrayToXMLBuffer(jsonArray, elementName) {
    if (jsonArray === null) {
        return Buffer.from('');
    }
    /* holds the buffers */
    const buffers = [];
    /* convert each element to an xml buffer */
    for (let i = 0; i < jsonArray.length; i += 1) {
        /* convert it to xml */
        const xmlString = jsonToXMLString(jsonArray[i], elementName);
        /* convert it to a buffer */
        const b = Buffer.from(`${xmlString}\n`, 'utf8');
        /* add to buffers array */
        buffers.push(b);
    }
    /* concat to one giant buffer */
    return Buffer.concat(buffers);
}

function getArcherObjects() {
    if (transportSettings.testingMode) {
        Object.assign(transportSettings, testingModeParams);
        transportSettings.tokens = testingModeTokens;
    } else {
        Object.assign(transportSettings, context.CustomParameters);
        transportSettings.tokens = context.Tokens;
    }
}

function BuildMessageArray() {
    if (messageLogMap.size > 0) {
        // eslint-disable-next-line prefer-spread
        return [].concat.apply([], [].concat.apply([], Array.from(messageLogMap)));
    }
    return [];
}

// let Archer know we are done.
function ReturnToArcher(err) {
    if (outputWriter.IsNewFile && outputWriter.fileHelper && outputWriter.fileHelper.fileIndex && outputWriter.fileHelper.fileIndex === 1) {
        outputWriter.writeItem('');
    }
    if (err) {
        LogError('Datafeed Failure due to error.');
        callback(BuildMessageArray(), { output: null, previousRunContext: JSON.stringify(transportSettings.previousRunContext) });
    } else {
        LogInfo('Sending Complete to Archer.');
        callback(null, { output: null, previousRunContext: JSON.stringify(transportSettings.previousRunContext) });
    }
    return Promise.resolve(true);
}

// write completed records to disk
function SendCompletedRecordsToArcher(cd, data) {
    return new Promise(resolve => {
        // don't write empty data
        if (transportSettings.debug) {
            LogInfo(`[${cd.callId}]   Sending ${data.length} to Archer`);
        }
        if (data && data.length > 0) {
            const xmlData = jsonArrayToXMLBuffer(data, cd.callId);
            outputWriter.writeItem(xmlData);
        }
        resolve(true);
    });
}

function AddDays(date, days) {
    const dat = date;
    dat.setDate(date.getDate() + days);
    return dat;
}

function UnixSecondsToDateTime(unixSeconds) {
    /* convert to date */
    const dt = new Date(unixSeconds * 1000);

    let hours = dt.getHours();
    let ampm = 'AM';

    /* translate hours */
    if (hours > 12) {
        ampm = 'PM';
        hours -= 12;
    }

    /* build the string */
    return `${pad(dt.getMonth() + 1, 2)}/${pad(dt.getDate(), 2)}/${pad(dt.getFullYear(), 4)} ${pad(hours, 2)}:${pad(dt.getMinutes(), 2)} ${ampm}`;
}

function GetStartDate() {
    let startDate = new Date('1970-01-10T00:00:00Z');

    if (transportSettings.ignoreLastRunTime.toLowerCase() !== 'true') {
        startDate = transportSettings.lastRunTime;
    } else if (transportSettings.startDate && transportSettings.startDate.length === 10) {
        startDate = new Date(`${transportSettings.startDate}T00:00:00Z`);
    } else if (transportSettings.startDate && transportSettings.startDate !== '') {
        startDate = new Date(transportSettings.startDate);
    }
    return startDate;
}

/** ******************************************************/
/** INIT
/********************************************************/
function ConstructCallContext() {

    const callContext = {
        callId: '',
        url: '',
        method: '',
        startIndex: 0,
        endIndex: parseInt(transportSettings.batchSize, 10),
        headers: {	
            'User-Agent': 'RSA ARCHER DATAFEED CLIENT',
			'Content-Type': 'application/json'
        },
        baseUrl: transportSettings.url,
        batchSize: parseInt(transportSettings.batchSize, 10)

    };

    if (transportSettings.dataSource === 'allcontacts') {
        callContext.callId = 'ALLCONTACTS';
        callContext.url = '/contentapi/Contacts';
        callContext.method = 'Get';
    } else {
        throw new Error(`[${transportSettings.dataSource}] invalid data source set`);
    }
    
    
    return callContext;
}

let apif = null; // will be instantiated from init()
class APIFramework {
    constructor(apiFrameWorkConfig, debug) {
        const queueConfig = Object.assign({}, APIFramework.DefaultConfig().queueConfig, apiFrameWorkConfig.queueConfig);
        this.params = Object.assign({}, APIFramework.DefaultConfig(), apiFrameWorkConfig);
        this.params.queueConfig = queueConfig;
        this.holdQueueMap = new Map();
        this.throttle = 0;
        this.stop = false;
        this.requestRamp = 0;
        this.intervalStartMS = new Date().getTime();
        this.intervalRequestCnt = 0;
        this.inCoolDown = false;
        this.debug = debug;

        this.baseRequest = request.defaults({
            forever: true,
            agentOptions: {
                keepAlive: true,
                keepAliveMsecs: 1000,
                maxSockets: this.params.socketLimit,
                maxFreeSockets: this.params.socketLimit
            },
            pool: {
                maxSockets: this.params.socketLimit
            },
            timeout: 120000
        });
    }

    // do not modify
    // use the constructor and config to pass in changes
    static DefaultConfig() {
        return {
            queueConfig: {
                default: {
                    key: 'default',
                    priority: 50
                }
            },
            verifyCerts: false,
            concurrencyLimit: 10,
            socketLimit: 10,
            maxRetry: 1,
            retryCodes: ['ECONNRESET'],
            requestsPerMinLimit: 60,
            proxy: ''
        };
    }

    CreateQueueMap(queueKey) {
        let queueConfig = this.params.queueConfig.default;
        if (this.params.queueConfig[queueKey]) {
            queueConfig = this.params.queueConfig[queueKey];
        }

        return { requestItems: [], queueKey, queueConfig };
    }

    CreateRequestItem(queueKey, req, reqCallBack, retryCount) {
        const qmap = this.holdQueueMap.get(queueKey);
        if (retryCount && retryCount <= this.params.maxRetry) {
            qmap.requestItems.unshift({ req, reqCallBack, retryCount });
        } else {
            qmap.requestItems.push({ req, reqCallBack, retryCount: 0 });
        }

        return qmap;
    }

    QueueItem(queueKey, req, reqCallBack) {
        if (!this.holdQueueMap.get(queueKey)) {
            this.holdQueueMap.set(queueKey, this.CreateQueueMap(queueKey));
        }
        this.CreateRequestItem(queueKey, req, reqCallBack);
        if (this.throttle <= this.params.concurrencyLimit && this.requestRamp <= 1) {
            this.requestRamp += 1;
            this.ExecuteNext();
        }
    }

    async webCall(url, method, headers, postBody) {
        /* build options */
        const options = {
            method,
            uri: url,
            headers,
            body: postBody,
            rejectUnauthorized: this.params.verifyCerts,
            forever: true,
            agentOptions: {
                keepAlive: true,
                keepAliveMsecs: 1000,
                maxSockets: 10,
                maxFreeSockets: 10
            },
            pool: {
                maxSockets: 10
            },
            timeout: 120000
        };

        /* add in proxy */
        if (this.params.proxy && this.params.proxy !== '') {
            options.proxy = this.params.proxy;
        }

        /* make the request */
        return new Promise((resolve, reject) => {
            this.baseRequest(options, function handleResponse(err, response, body) {
                /* check for error */
                if (err) {
                    let errorToCapture = `WEB CALL ERROR:         ${err} \n`;
                    errorToCapture += err.code === 'ETIMEDOUT' ? `[${err.connect}] T-Connection Timeout, F-Read Timeout\n` : ' ';
                    errorToCapture += `WEB CALL ERROR HEADERS: ${JSON.stringify(headers)} \n WEB CALL ERROR BODY:    ${body}`;
                    LogError(errorToCapture);
                    return reject(err);
                }

                if (response.statusCode !== 200) {
                    let errorMsg = `INVALID HTTP ERROR CODE RETURNED : ${response.statusCode}\n`;
                    errorMsg += `ERROR HEADERS: ${JSON.stringify(headers)}\n`;
                    errorMsg += `ERROR BODY:    ${body}`;
                    LogError(errorMsg);
                    return reject(response);
                }
                const resHeaders = response.headers ? response.headers : [];
                return resolve({ resHeaders, body });
            });
        });
    }

    static async delay(ms) {
        return new Promise(resolve => {
            if (ms) {
                // try again after period of time
                setTimeout(() => resolve(), ms);
            } else {
                // try again on next event loop cycle
                setImmediate(() => resolve());
            }
        });
    }

    Stop(resolvePromises = true) {
        this.stop = true;
        this.holdQueueMap.forEach(qMap => {
            while (qMap.requestItems.length > 0) {
                const r = qMap.requestItems.shift();
                if (resolvePromises) {
                    r.reqCallBack(null, null);
                } else {
                    r.reqCallBack(new Error('Stop Requested'));
                }
            }
        });
    }

    async GetNextItem() {
        while (this.throttle >= this.params.concurrencyLimit || this.inCoolDown) {
            if (this.stop) {
                return null;
            }
            // This is an exception
            // Recursion in this case would lead to a large memory hogging stack
            // eslint-disable-next-line no-await-in-loop
            await APIFramework.delay();
        }

        let queue = null;
        let highestPriority = Infinity; // highest number is least prioritized
        this.holdQueueMap.forEach(qMap => {
            if (qMap.queueConfig.priority < highestPriority && qMap.requestItems.length > 0) {
                highestPriority = qMap.queueConfig.priority;
                queue = qMap;
            }
        });

        let item = null;
        if (queue) {
            this.throttle += 1;
            this.intervalRequestCnt += 1;
            item = queue.requestItems.shift(); // FIFO

            const coolDownMS = this.coolDownCheck();
            // check if we need to slow down.
            // only check for a slow down if we have something to do.
            if (coolDownMS > 0) {
                this.inCoolDown = true;
                // wait for a minimum of 1 sec
                if (this.debug) {
                    LogInfo(`[APIF]   Cooling ${Math.max(coolDownMS, 1000)}`);
                }
                await APIFramework.delay(Math.max(coolDownMS, 1000));
                this.inCoolDown = false;
            }
        }

        return { item, queue };
    }

    coolDownCheck() {
        const nowMS = new Date().getTime();
        let elapsedMS = nowMS - this.intervalStartMS;
        if (this.debug) {
            LogInfo(`[APIF]   Elapsed Milliseconds ${elapsedMS}`);
        }
        // reset interval for rate calculation if we exceeded interval
        // calculations are based on one minute intervals
        if (elapsedMS > 60000) {
            this.intervalStartMS = new Date().getTime();
            this.intervalRequestCnt = 1; // the only way we got here is because we have something to do
            elapsedMS = 500; // default to half second to ensure no division by zero
            if (this.debug) {
                LogInfo(`[APIF]   Reset request interval`);
            }
        }
        const effectiveRatePerMin = this.intervalRequestCnt / (elapsedMS / 60000);

        // amount we need to slow down to stay within the requestsPerMinLimit threshold at our current effective rate
        // default to zero since negative numbers are useless
        // take the overage as a percentage of the elapsed time to get the amount to slow down
        return Math.max((effectiveRatePerMin / this.params.requestsPerMinLimit - 1) * elapsedMS, 0);
    }

    async queueRetry(reqMap) {
        await APIFramework.delay();
        this.CreateRequestItem(reqMap.queue.queueKey, reqMap.item.req, reqMap.item.reqCallBack, reqMap.item.retryCount + 1);
    }

    async ExecuteNext() {
        const reqMap = await this.GetNextItem();

        if (!reqMap || !reqMap.item || !reqMap.item.req) {
            this.requestRamp = 0;
            return;
        }
        try {
            if (this.debug) {
                LogInfo(`[APIF]   Sending call`);
            }
            const p = reqMap.item.req();
            // trigger the next call
            this.ExecuteNext();

            const data = await p;
            this.throttle = Math.max(this.throttle - 1, 0); // ensure we don't ever go below zero
            if (this.debug) {
                LogInfo(`[APIF]   Returning data`);
            }
            reqMap.item.reqCallBack(null, data);
        } catch (error) {
            this.throttle = Math.max(this.throttle - 1, 0); // ensure we don't ever go below zero
            if (error.code && this.params.retryCodes.includes(error.code.toUpperCase()) && reqMap.item.retryCount < this.params.maxRetry) {
                this.queueRetry(reqMap);
            } else {
                this.requestRamp = 0;
                reqMap.item.reqCallBack(error);
            }
        }
    }

    // Entry point
    QueueWebCall(queueKey, url, method, headers, postBody) {
        return new Promise((resolve, reject) => {
            // Store without calling
            this.QueueItem(
                queueKey,
                () => this.webCall(url, method, headers, postBody),
                (error, data) => {
                    if (error) {
                        return reject(error);
                    }
                    return resolve(data);
                }
            );
        });
    }
}

function Init() {
    return new Promise((resolve, reject) => {
        try {
            /* run the feed */
            LogInfo('Datafeed Init');
            /* check if testing mode should be active (no archer DFE present) */
            if (typeof context === 'undefined' || typeof context.CustomParameters === 'undefined' || Object.keys(context.CustomParameters).length === 0) {
                LogWarn('Testing Mode Active');
                transportSettings.testingMode = true;
            }
            /* get params and tokens */
            getArcherObjects();

            /* setup cert verify. This is a failsafe to ensure that we do not verify certs */
            if (transportSettings.verifyCerts.toLowerCase() === 'false') {
                process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
            }

            /* setup proxy */
            if (transportSettings.proxy != null && transportSettings.proxy !== '') {
                transportSettings.useProxy = true;
            }

            /* check for last run time */
            if ('LastRunTime' in transportSettings.tokens && transportSettings.tokens.LastRunTime !== null && transportSettings.tokens.LastRunTime !== '') {
                transportSettings.lastRunTime = new Date(transportSettings.tokens.LastRunTime);
            }
            LogInfo(`Last Datafeed Run Time: ${transportSettings.lastRunTime.toISOString()}`);

            // NEW
            const callContext = ConstructCallContext();

            LogInfo(`Using StartIndex:  ${callContext.startIndex}`);
            LogInfo(`Using EndIndex:    ${callContext.endIndex}`);

            // these override the defaults in the APIFramework
            const APIFrameWorkConfig = {
                verifyCerts: transportSettings.verifyCerts.toLowerCase() === 'true',
                requestsPerMinLimit: parseInt(transportSettings.requestsPerMin, 10),
                proxy: transportSettings.proxy
            };

            apif = new APIFramework(APIFrameWorkConfig, transportSettings.debug);

            return resolve(callContext);
            // end NEW
        } catch (error) {
            CaptureError(error);
            return reject(error);
        }
    });
}

/** ******************************************************/
/** DATAFEED BEGINS HERE
/********************************************************/

async function GetNextPage(cd, queryString, postData) {
    if (transportSettings.debug) {
        LogInfo(`Downloading ${cd.startIndex} through ${cd.endIndex}`);
    }
    /* make the call */
    return apif.QueueWebCall('default', cd.baseUrl + cd.url + queryString, cd.method, cd.headers, postData);
}

/** ******************************************************/
/** Feed Related Constants
/********************************************************/
const dataToClean = {
    dates: [],
    miscSpace: []
};
const xrefURLMap = {
    mskb: 'https://urldefense.com/v3/__https://support.microsoft.com/en-us/help/*7BID*7D__;JSU!!LpKI!xrs2AeBPQlOZW9YPOTyXTY18IwuN5RwhzFW3MtE_HJ8mxJFfiiFgRw6a0qg-VYA$ [support[.]microsoft[.]com]',
    cwe: 'https://urldefense.com/v3/__http://cwe.mitre.org/data/definitions/*7BID*7D__;JSU!!LpKI!xrs2AeBPQlOZW9YPOTyXTY18IwuN5RwhzFW3MtE_HJ8mxJFfiiFgRw6aQKWGkA0$ [cwe[.]mitre[.]org]',
    secunia: 'https://urldefense.com/v3/__http://secunia.com/advisories/*7BID*7D__;JSU!!LpKI!xrs2AeBPQlOZW9YPOTyXTY18IwuN5RwhzFW3MtE_HJ8mxJFfiiFgRw6as5Xr7aw$ [secunia[.]com]',
    cve: 'https://urldefense.com/v3/__http://web.nvd.nist.gov/view/vuln/detail?vulnId=*7BID*7D__;JSU!!LpKI!xrs2AeBPQlOZW9YPOTyXTY18IwuN5RwhzFW3MtE_HJ8mxJFfiiFgRw6a7uTGw24$ [web[.]nvd[.]nist[.]gov]'
};

/** ******************************************************/
/** DATA Parsers
/********************************************************/
function DataCleanse(rec) {
    const cleanedRec = rec;
    dataToClean.miscSpace.forEach(k => {
        if (cleanedRec[k]) {
            cleanedRec[k] = cleanedRec[k].replace(/\$/g, '');
        }
    });

    dataToClean.dates.forEach(k => {
        if (cleanedRec[k]) {
            cleanedRec[k] = UnixSecondsToDateTime(parseInt(cleanedRec[k], 10));
        }
    });
    return cleanedRec;
}

async function ContactDataParser(cd, data) {
	//var stringifyData = JSON.stringify(data);
	//stringifyData = stringifyData.replace('urn:ietf:params:scim:schemas:extension:enterprise:2.0:User','Enterprise');
	//var jsonOBJ = JSON.parse(stringifyData);
    const jsonData = JSON.parse(data.body);
    if (!jsonData.value) {
        return [];
    }
	// capture distinct user data 
	var contactsToUpdate = [];
    var usersToCreate = [];
	var usersToInactivate = [];

	usersToCreate = jsonData.value.filter(function (entry) {
		return entry.RSA_Archer_User_Account.length === 0;
	});
	
	usersToInactivate = jsonData.value.filter(function (entry) {
		return entry.Employment_Status[0] === 'Inactivo';
	});
	usersToInactivate.forEach(d => {
        // these are all we care about for usersToInactivate
        let cleanedRecToInactivate = (({RSA_user_ID}) => ({
			RSA_user_ID
        }))(d);
		var postBody = {};
		var jsBody = JSON.stringify(postBody);
		apif.QueueWebCall('default', `${cd.baseUrl}/platformapi/core/system/user/status/inactive/${cleanedRecToInactivate.RSA_user_ID}`, 'POST', cd.headers, jsBody); 
    });
	
    usersToCreate.forEach(d => {
		
        // these are all we care about for userAccounts
        let cleanedRecToCreate = (({ Email_Business, Employment_Status, Name_First,Name_Last,Title,RSA_Archer_User_Account,ID_de_grupo_Archer}) => ({
            Email_Business,
            Employment_Status,
            Name_First,
			Name_Last,
			Title,
			RSA_Archer_User_Account,
			ID_de_grupo_Archer
        }))(d);
		var postBody = {
			"User":{
				"FirstName":cleanedRecToCreate.Name_First,
				"LastName":cleanedRecToCreate.Name_Last,
				"UserName":cleanedRecToCreate.Email_Business,
				"Locale":"es-CL",
				"TimeZoneId":"Pacific SA Standard Time",
				"Title":cleanedRecToCreate.Title,
				"AccountStatus":"1"
			},
			"Password":"NewUser2005!",
			"Contacts":
				[ 
					{ 
						"ContactType":7,
						"ContactSubType":2,
						"Value":cleanedRecToCreate.Email_Business,
						"IsDefault":true
					}
				],
			"Roles":[1],
			"Groups":[cleanedRecToCreate.ID_de_grupo_Archer]	
		};
		var jsBody = JSON.stringify(postBody);
		apif.QueueWebCall('default', `${cd.baseUrl}platformapi/core/system/user`, 'POST', cd.headers, jsBody);

		const emailAddress = `${d.Email_Business}`;
		cleanedRecToCreate.RSA_Archer_User_Account=emailAddress;
        // add to output 
        contactsToUpdate.push(cleanedRecToCreate);
    });
	
	return contactsToUpdate;
    // return parsed data 
   // return contacts;
	//return jsonData.value;
}

async function FilterData(cd, data) {
    return data.filter(d => {
        const modDate = d.modifiedTime ? new Date(d.modifiedTime) : new Date();
        return modDate >= cd.startDate;
    });
}

async function PageContinueCheck(cd, parsedData) {
    const nextCd = cd;
    if (parsedData && parsedData.length > 0) {
        nextCd.startIndex += cd.batchSize;
        nextCd.endIndex += cd.batchSize;
        return { nextCd, done: false };
    }
    return { nextCd, done: true };
}

/** ******************************************************/
/** Main
/********************************************************/
async function GetAllPages(callContext) {
    const firstCallContext = callContext;

    async function CacheAuthInfo(authResult) {
  
        const body = JSON.parse(authResult.body);
        /* check for release key 

        /* get the session token */
		//LogInfo(`body ${JSON.stringify(body)}`);
        const token = body.RequestedObject["SessionToken"];
        /* add token to headers */
        firstCallContext.headers['Authorization'] = "Archer session-id="+token.toString();
        return Promise.resolve(true);
    }
	

    async function Authenticate(instance, username, password) {
        /* build json object */
        const postBody = {"InstanceName":instance, "Username":username, "UserDomain":"", "Password":password };
        const jsBody = JSON.stringify(postBody);
		const authHeader = firstCallContext.headers;
		
        const authResult = apif.QueueWebCall(
            'default',
            `${firstCallContext.baseUrl}/platformapi/core/security/login`,
            'POST',
            authHeader,
            jsBody
        );

        return authResult.then(resData => CacheAuthInfo(resData));
    }
		
    // Pagination
    async function ProcessPages(dataFN) {
        const DoChain = async (cd, acc) => {
            const p = dataFN(cd);
            acc.push(p);
            return p.then(
                // what to accumulate??
                ({ nextCd, done }) => (done ? acc : DoChain(nextCd, acc))
            );
        };

        // start the first pass
        return DoChain(firstCallContext, []);
    }

    await Authenticate(transportSettings.instance, transportSettings.username, transportSettings.password);

    // CONTACTS
    if (firstCallContext.callId === 'ALLCONTACTS') {	
        return ProcessPages(async cd => {
            const queryString = `${'?skip='}${cd.startIndex}`;
            const data = await GetNextPage(cd, queryString, null);

            const parsedData = await ContactDataParser(cd, data);
            //const filteredData = await FilterData(cd, parsedData);
            await SendCompletedRecordsToArcher(cd, parsedData);
            return PageContinueCheck(cd, parsedData);
        });
    }

}

/** ******************************************************/
/** DATAFEED BEGINS HERE
/********************************************************/

Init()
    .then(callContext => {
        return GetAllPages(callContext);
    })
    .then(() => {
        return ReturnToArcher(false);
    })
    .catch(e => {
        CaptureError(e);
        ReturnToArcher(true);
    });
